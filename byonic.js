const ELEMENTS_TO_SKIP = [
  'H1',
  'H2',
  'H3',
  'H4',
  'H5',
  'H6',
  'B',
  'STRONG',
  'TH',
  'CODE',
  'PRE',
  'SAMP'
];

const ESCAPED_CHARACTERS = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
};

const WORD_SEPARATORS = /(?=[\s/-])/;

const goByonic = (topElement) => {
  injectByonicStyle();
  convertToByonicText(topElement);
}

const injectByonicStyle = () => {
  const newStyle = document.createElement('style');
  newStyle.innerHTML = '.byonic { font-weight: bolder; }'
  const head = document.querySelector('head');
  head.append(newStyle);
};

const convertToByonicText = (element = document.body) => {
  if (ELEMENTS_TO_SKIP.includes(element.tagName)) {
    return;
  }

  Array.from(element.childNodes).forEach(childNode => {
    switch (childNode.nodeType) {
      case Node.TEXT_NODE:
        const byonicElement = createByonicElementFromNode(childNode);
        element.replaceChild(byonicElement, childNode);
        break;
      case Node.ELEMENT_NODE:
        convertToByonicText(childNode);
    }
  });
};

const createByonicElementFromNode = (node) => {
  let byonicText = '';
  let originalText = node.textContent;
  for (const character in ESCAPED_CHARACTERS) {
    originalText = originalText.replaceAll(new RegExp(character, 'g'), ESCAPED_CHARACTERS[character]);
  };
  let words = originalText.split(WORD_SEPARATORS);
  words.forEach(word => {
    const wordLength = word.length + 1; // Compensate for character
    const boldLength = Math.ceil(wordLength / 2);
    const [boldText, notBoldText] = [word.slice(0, boldLength), word.slice(boldLength)];
    byonicText += `<span class="byonic">${boldText}</span>${notBoldText}`;
  });
  const byonicElement = document.createElement('span');
  byonicElement.insertAdjacentHTML('afterbegin', byonicText.trim() + ' ');
  return byonicElement;
};

// Split by character, but then replace that character afterwards
